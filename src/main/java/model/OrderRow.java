package model;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;
import lombok.NonNull;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Embeddable
public class OrderRow {

    @NonNull
    @Column(name = "item_name")
    private String itemName;

    @NotNull
    @Min(1)
    private int quantity;

    @NotNull
    @Min(1)
    private int price;

}
