package app;

import conf.HsqlDataSource;
import model.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import javax.validation.Valid;
import java.util.List;

@RestController
public class OrderController {

    @Autowired
    private OrderDao dao;

    @Autowired
    private InstallmentService installmentService;


    @GetMapping("orders")
    public List<Order> getOrders() {
        return dao.getAllOrders();
    }

    @PostMapping("orders")
    public Order saveOrder(@RequestBody @Valid Order order) {
        return dao.insertOrder(order);
    }

    @DeleteMapping("orders/{id}")
    public void deleteOrder(@PathVariable Long id) {
        dao.deleteOrder(id);
    }

    @GetMapping("orders/{id}/installments")
    public List<Installment> createInstallments(@PathVariable Long id, @RequestParam("start") String startDate,
                                   @RequestParam("end") String endDate) {
         Order order = dao.findOrderById(id);
         return installmentService.createInstallments(order.getOrderRows(), startDate, endDate);
    }

    @GetMapping("orders/{id}")
    public Order getOrder(@PathVariable Long id) {
        return dao.findOrderById(id);
    }
}
