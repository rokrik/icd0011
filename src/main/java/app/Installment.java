package app;

import lombok.Data;

@Data
public class Installment {

    public int amount;

    public String date;

    public Installment(int amount, String date) {
        this.amount = amount;
        this.date = date;
    }

}
