package app;

import model.OrderRow;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

@Service
public class InstallmentService {

    public List<Installment> createInstallments(List<OrderRow> orderRows, String startDate, String endDate) {
        List<Installment> installments = new ArrayList<>();

        int totalInstallments = (int) ChronoUnit.MONTHS.between(
                LocalDate.parse(startDate).withDayOfMonth(1),
                LocalDate.parse(endDate).withDayOfMonth(1)
        ) + 1;

        int totalPrice = 0;
        for (OrderRow orderRow: orderRows) {
            totalPrice += orderRow.getQuantity() * orderRow.getPrice();
        }
        int currentAmount = totalPrice / totalInstallments;
        if (currentAmount < 3) {
            currentAmount = 3;
            totalInstallments = totalPrice / currentAmount;
        }

        String currentDate = startDate;
        String currentMonth = currentDate.substring(5, 7);
        String currentYear = currentDate.substring(0, 4);
        for (int i = 0; i < totalInstallments; i++) {
            if (i == totalInstallments - 2 && totalPrice % totalInstallments != 0) {
                currentAmount += (totalPrice % totalInstallments) / 2;
            }
            installments.add(new Installment(currentAmount, currentDate));
            int currentMonthInteger = Integer.parseInt(currentMonth);

            if (currentMonthInteger < 12) {
                if (currentMonthInteger < 9) {
                    currentMonth = "0" + (currentMonthInteger + 1);
                } else {
                    currentMonth = String.valueOf(currentMonthInteger + 1);
                }
            } else {
                currentYear = String.valueOf(Integer.parseInt(currentYear) + 1);
                currentMonth = "01";
            }
            currentDate = currentYear + "-" + currentMonth + "-01";
        }
        return installments;
    }

}
